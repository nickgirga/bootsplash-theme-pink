[bootsplash](https://github.com/dreemurrs-embedded/Pine64-Arch/tree/20200907/PKGBUILDS/pine64/bootsplash-danctnix) theme for [Pine64-Arch](https://github.com/dreemurrs-embedded/Pine64-Arch) with a pink material Arch logo and spinner.

### Intended for use with [Pine64-Arch](https://github.com/dreemurrs-embedded/Pine64-Arch). Installation on other systems may cause fatal breakage.

# Obtaining
You may simply click on [releases](https://gitlab.com/nickgirga/bootsplash-theme-pink/-/releases) and download the latest `.pkg.tar.xz` file (a built package) or you can clone the whole repository using `git clone https://gitlab.com/nickgirga/bootsplash-theme-pink.git` (all source files and a built package).

# Building
This step is only necessary if you are not using a built package. Simply run `makepkg -f` to rebuild the package using the source files.

# Installation
If using a built package, your GUI package manager should be able to install it for you (I have only tested using [pamac-aur](https://aur.archlinux.org/packages/pamac-aur/)). Opening the file from a file browser should be sufficient. Just tap install, enter your password, and wait for it to complete. Reboot to apply the changes. If your GUI package manager is incapable of the job, you can install it using `pacman -U [package_name].pkg.tar.xz`. This will replace `bootsplash-theme-danctnix-1-0`. The original theme can be built from [here](https://github.com/dreemurrs-embedded/Pine64-Arch/tree/master/PKGBUILDS/danctnix/bootsplash-danctnix). If you cloned the whole repository, you may also use `makepkg -i` to install your built package (or the included one). If you wish to rebuild the package before install, the `makepkg -fi` command will do it all in one line.

# Customization
The splash logo is 254x381 pixels in a transparent PNG format. You can replace it the easy way by renaming your new PNG `danctnix.png` and replacing the original. The loading spinner is 32x32 pixels in an animated GIF format that is 30 frames long (taking 1 second to cycle). You can replace this one the easy way by renaming your new GIF `spinner.gif` and replacing the original. Feel free to use the included images wherever you would like.

## Images
##### Splash Logo
![Splash Logo](/danctnix.png)

##### Splash Spinner
![Splash Spinner](/spinner.gif)

## Screenshots
##### Simulated Splash Screenshot
![Simulated Splash Screenshot](.screenshots/splash.png)
